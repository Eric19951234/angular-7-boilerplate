import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class APIService {

  constructor(
    public httpClient: HttpClient,
  ) {
  }

  async getCountriesAPI(continent: string): Promise<any> {
    return this._getCountriesAPI(continent)
      .then(res => {
        let allCountries: any[] = [];
        let filteredCountries = [];
        allCountries = res;
        allCountries.forEach(doc => {
          let massagedObj = {
            name: doc.name,
            region: doc.region
          };
          filteredCountries.push(massagedObj);
        })
        return filteredCountries;
      })
  }

  private _getCountriesAPI(continent: string): Promise<any> {
    return this.httpClient.get(
      `https://restcountries.eu/rest/v2/region/${continent}`
    ).toPromise()
  }

}
