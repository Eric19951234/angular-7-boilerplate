import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { APIService } from '../../services/api.service';

import { DummyData } from '../../interfaces/dummyData.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.html',
  styleUrls: ['./home.scss']
})
export class HomePage implements OnInit {

  public dummyData: DummyData;

  constructor(
    private _api: APIService,
    private _router: Router,
  ) {
  }

  ngOnInit() {
  }

  apiTest() {
    this._api.getCountriesAPI('Africa').then(
      res => console.log('res', res)
    )
  }

  goToTestPage() {
    this._router.navigate(['/test']);
  }

}
