import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TestPage } from './test';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  declarations: [
    TestPage,
  ],
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: TestPage
      },
    ]),
    SharedModule
  ],
})
export class TestPageModule { }
