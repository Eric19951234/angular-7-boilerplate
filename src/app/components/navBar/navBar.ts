import { Component, Input } from '@angular/core';

@Component({
  selector: 'nav-bar',
  templateUrl: './navBar.html',
  styleUrls: ['./navBar.scss']
})

export class NavBarComponent {
  @Input() title: string;

  constructor() {
  }

  ngOnInit() {
  }

}
